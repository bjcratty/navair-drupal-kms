<?php

namespace Drupal\devel_php\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that allows privileged users to execute arbitrary PHP code.
 */
class ExecutePHP extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'devel_execute_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $emptyResults = '<h4>Results</h4>' . "<div id=\"dbTable\" class=\"myAPITable tableFixHead\">" . "<table id=\"dbTable\" >" ;
    $emptyResults .= '<tr><th>NULL</th></tr><tr><td>N/A</td></tr></table></div>';
    
    $form = array(
      '#title' => $this->t('Execute MySQL'),
      '#description' => $this->t('Execute some MySQL'),
    );
    $form['execute']['code'] = array(
      '#type' => 'textarea',
      '#title' => t('MySQL to execute'),
      '#description' => t('Enter a MySQL query. If the query consists of errors, please refer to the error page that appears after executing query. Note: An error message will appear even if the table or view exists but there is no data inside it. Use Show Tables to view creation'),
      '#default_value' => (isset($_SESSION['devel_execute_code']) ? $_SESSION['devel_execute_code'] : ''),
      '#rows' => 20,
    );
    $form['execute']['op'] = array('#type' => 'submit', '#value' => t('Execute'));
    $form['execute']['output'] = array(
      '#type' => 'markup',
      // '#title' => t('MySQL to execute'),
      // '#description' => t('Enter some code. Do not use <code>&lt;?php ?&gt;</code> tags.'),
      '#markup' => (isset($_SESSION['mysql_output']) ? $_SESSION['mysql_output'] : $emptyResults)
      // '#default_value' => (isset($_SESSION['devel_execute_code']) ? $_SESSION['devel_execute_code'] : ''),
    );



    $form['#attached']['library'][] = 'devel_php/MyDevel';

    $form['#redirect'] = FALSE;

    if (isset($_SESSION['devel_execute_code'])) {
      unset($_SESSION['devel_execute_code']);
    }
    if (isset($_SESSION['mysql_output'])) {
      unset($_SESSION['mysql_output']);
    }
    
    $str="SHOW TABLES LIKE 'Database_AUDIT'";
    $connection = \Drupal::database();
    $query = $connection->query($str);
    $result = $query->fetchAll();
    $array = $result;
    // if(empty($array)){
    //   $str="CREATE TABLE Database_AUDIT(
    //     id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    //     QueryString VARCHAR(30) NULL,
    //     LoggedMessage BLOB NULL,
    //     LoggedDate TIMESTAMP
    //     )";
    //   $connection = \Drupal::database();
    //   $query = $connection->query($str);
    //   //$result = $query->fetchAll();
    //  // $array = $result;
    // }


    return $form;


  }

   /**
     * (@inheritdoc)
     */
    public function validateForm(array &$form, FormStateInterface $form_state){
      // $msg ="TEST";
      $queryString = $form_state->getValue('code');
      // $form_state->setErrorByName('code', $msg);

      if (strpos($queryString, 'create') !== false){
          $form_state->setErrorByName('code', t('Query is contains Create. Read only querires are allowed', array('%code' => $queryString)));
      }

      if (strpos($queryString, 'update') !== false) {
        $form_state->setErrorByName('code', t('Query is contains Update. Read only querires are allowed', array('%code' => $queryString)));
      }

      if (strpos($queryString, 'insert') !== false) {
        $form_state->setErrorByName('code', t('Query is contains Insert. Read only querires are allowed', array('%code' => $queryString)));
      }

      if (strpos($queryString, 'delete') !== false) {
        $form_state->setErrorByName('code', t('Query is contains Delete. Read only querires are allowed', array('%code' => $queryString)));
      }

      if (strpos($queryString, 'alter') !== false) {
        $form_state->setErrorByName('code', t('Query is contains Alter. Read only querires are allowed', array('%code' => $queryString)));
      }
      try {
        $queryString = $form_state->getValue('code');
        $connection = \Drupal::database();
        // $query = $connection->query("SELECT title FROM resourceExport_V3");
        $query = $connection->query($queryString);
        $result = $query->fetchAll();
        $array = $result;
        }
        catch (Exception $e) {
          \Drupal::logger('type')->error($e->getMessage());
          // $msg = $e-getMessage();
        }

      if(empty($array)){
        $form_state->setErrorByName('code', t('Query returned nothing', array('%code' => $queryString)));
      }

      // if (!mysqli_query($connection, $queryString)) {
      //   $form_state->setErrorByName('code', mysqli_errno($connection)); 
      // }

      // \Drupal::logger('type')->error($e->getMessage());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $msg = "No Message Recorded";
    $code = $form_state->getValue('code');
    try {
      ob_start();
      // $dbStr = $form_state->getValue('')
      // $form_state->setRebuild();

      $connection = \Drupal::database();
      // $query = $connection->query("SELECT title FROM resourceExport_V3");
      $query = $connection->query($code);
      $result = $query->fetchAll();


      $array = $result;
      //"<div style= 'width: 100% overflow-x: scroll'>"
      $html = '<h4>Results</h4>' . "<div id=\"dbTable\" class=\"myAPITable tableFixHead\">" . "<table id=\"dbTable\" >" ;
      // header row
      $html .= '<tr>';
      foreach($array[0] as $key=>$value){
              $html .= '<th>' . htmlspecialchars($key) . '</th>';
          }
      $html .= '</tr>';

      // data rows
      foreach( $array as $key=>$value){
          $html .= '<tr>';
          foreach($value as $key2=>$value2){
              $html .= '<td>' . htmlspecialchars($value2) . '</td>';
          }
          $html .= '</tr>';
      }

      // finish table and return it

      $html .= '</table></div>';




      // print eval($code);
      $_SESSION['devel_execute_code'] = $code;
      $_SESSION['mysql_output'] = $html;

      dpm(ob_get_clean());
  } 
  catch (Exception $e) {
    \Drupal::logger('type')->error($e->getMessage());
    \Drupal::logger('devel_php')->error($e->getMessage());
  }

    $logInfo = "INSERT INTO Database_AUDIT (QueryString, LoggedMessage) 
    VALUES ( '" . $code . "' , '" . $msg . "' );";
    $connection = \Drupal::database();
    // $query = $connection->query("SELECT title FROM resourceExport_V3");
    $query = $connection->query($logInfo);
    // $result = $query->fetchAll();
  }
}
