<?php
/**
 * @file
 * Contains \Drupal\myAPI\Controller\MyModuleController.
 */

namespace Drupal\myAPI\Controller;

use Drupal\Core\Controller\ControllerBase;
//use Drupal\Core\Database\Database;

use Drupal\node\Entity\Node;

class FirstController extends ControllerBase  {
    public function build_table($array){
        // start table
        $html = '<table>';
        // header row
        $html .= '<tr>';
        foreach($array[0] as $key=>$value){
                $html .= '<th>' . htmlspecialchars($key) . '</th>';
            }
        $html .= '</tr>';
    
        // data rows
        foreach( $array as $key=>$value){
            $html .= '<tr>';
            foreach($value as $key2=>$value2){
                $html .= '<td>' . htmlspecialchars($value2) . '</td>';
            }
            $html .= '</tr>';
        }
    
        // finish table and return it
        $html .= '</table>';
        return $html;
    }

    public function content()  {
        //$this->sessionManager->regenerate();
        // $url = "http://myresources.dd:8083/jsonapi/node/resources";
        // $maps_json = file_get_contents($url);
        // $json = json_decode($maps_json);
        // $output = array();
        // foreach ($json->data as $item) 
        // {
        //     array_push($output, $item);//->body->processed);//array($item->attributes->title, $item->body, $item->field_source->url));
        //     //echo '<div>' . var_dump($item->attributes->title) . '</div>';
        // }

        //  foreach($json['data'] as $item) {
        //     echo 'Title: ' . $item['attributes']['title'] . '<br />';
        //     echo 'Body: ' . $item['body']['value'] . '<br />';
        //     echo 'Source: ' . $item['field_source']['url'] . '<br />';
        // }
         //$myJSON = json_encode($myObj);
        //return $info;
        $connection = \Drupal::database();
        //$sql = "SELECT node__field_keywords.entity_id, node__body.body_value, node__field_source.field_source_url, GROUP_CONCAT(node__field_keywords.field_keywords_target_id SEPARATOR ', ') as 'field_keywords' FROM node__field_keywords left join node__field_source on node__field_keywords.entity_id = node__field_source.entity_id left join node__body on node__field_keywords.entity_id = node__body.entity_id GROUP BY node__field_keywords.entity_id";
        // $test = "SELECT
        // node__field_keywords.entity_id, 
        // node__body.body_value,
        // node__field_source.field_source_url,
        // GROUP_CONCAT(node__field_keywords.field_keywords_target_id SEPARATOR ', ') as 'field_keywords'
        // FROM node__field_keywords
        // left join node__field_source on node__field_keywords.entity_id = node__field_source.entity_id
        // left join node__body on node__field_keywords.entity_id = node__body.entity_id
        // GROUP BY node__field_keywords.entity_id";
        // $sql = $connection->select('node__field_keywords', 'k');
        // $sql->addExpression();
        // $sql->leftJoin('node__field_source', 's', 'k.entity_id = s.entity_id');
        // $sql->leftJoin('node__body', 'b', 'k.entity_id = b.entity_id');
        // $sql->join('node_field_data', 'd', 'd.nid = k.entity_id');

        //$query = $connection->query($sql);
        //$result = $sql->fetchAll();

        // $result = $sql->fields('k', ['entity_id'])
        //                 ->fields('d', ['title'])
        //                 ->fields('b', ['body_value'])
        //                 ->fields('s', ['field_source_url'])
        //                 ->execute();

        //echo "<pre>" . print_r($result) . "</pre>";

        //$result = db_query("SELECT node__field_keywords.entity_id, node__body.body_value, node__field_source.field_source_url, GROUP_CONCAT(node__field_keywords.field_keywords_target_id SEPARATOR ', ') as 'field_keywords' FROM node__field_keywords left join node__field_source on node__field_keywords.entity_id = node__field_source.entity_id left join node__body on node__field_keywords.entity_id = node__body.entity_id GROUP BY node__field_keywords.entity_id");
        //$connection = \Drupal::database();
        //Database::getConnection('external');
        // \Drupal\Core\Database\Database::setActiveConnection('external');
        $connection = \Drupal::database();
        // $connection = Database::getConnection();
        //echo "<pre>".print_r($connection)."</pre>";
        $changeDB = $connection->query("USE MyResources");
        //$changed = $changeDB->fetchAll();
        $query = $connection->query("SELECT * FROM v_resources_2");
        //$query = $connection->query("select * from node__body where bundle = 'dev_article'");
        $result = $query->fetchAll();
        $changeDB = $connection->query("USE MyResources_clone");


        $array = $result;
        //"<div style= 'width: 100% overflow-x: scroll'>"
        $html = '<div class' . '=myAPITable' . '><table>';
        // header row
        $html .= '<tr>';
        foreach($array[0] as $key=>$value){
                $html .= '<th>' . htmlspecialchars($key) . '</th>';
            }
        $html .= '</tr>';
    
        // data rows
        foreach( $array as $key=>$value){
            $html .= '<tr>';
            foreach($value as $key2=>$value2){
                $html .= '<td>' . htmlspecialchars($value2) . '</td>';
            }
            $html .= '</tr>';
        }
    
        // finish table and return it
    
        $html .= '</table></div>';

        //CUSTOM NODE CREATED EXAMPLE
        // $node = Node::create(['type' => 'resources']);
        // $node->title = 'Testing Removing Set';
        // //Body can now be an array with a value and a format.
        // //If body field exists.
        // $body = [
        // 'value' => '<h1>TESTING URL</h1>',
        // 'format' => 'basic_html',
        // ];
        // $node->set('body', "Testing Testing");
        // $node->set('uid', 1);
        // $node->field_source->url = "https://www.google.com";
        // //$node->field_keywords = $array[1]["field_keywords"];
        // //CREATES MULTIPLE TAXONOMY TERMS FOR THE NODE
        // $node->get('field_keywords')->appendItem(['target_id' => 2]);
        // $node->get('field_keywords')->appendItem(['target_id' => 16]);
        // $node->status = 1;
        // $node->enforceIsNew();
        // $node->save();
        // drupal_set_message( "Node with nid " . $node->id() . " saved!\n");



        //TURN ARRAY TO STRING
        // $arr2 = array_column($array, 'field_keywords');
        // // echo "<pre>".print_r($arr)."</pre>";
        // $terms_String=$arr2[1];
        // $x=explode(",",$str);
        // echo "<pre>".print_r($x)."</pre>";

        //DELETE ALL RESOURCE NODES TO TEST
        // $nodes = \Drupal::entityTypeManager()
        //     ->getStorage('node')
        //     ->loadByProperties(array('type' => 'resources'));

        // foreach ($nodes as $node) {
        //     $node->delete();
        // }


        //LOOP THROUGH ALL THE RESULTS
        // foreach($array as $newNode){
           
        // }
        //CREATE THE DATA ARRAYS WE NEED

        $currentContent = $connection->query("SELECT `type`, `title` from node_field_data");
        $contents = $currentContent->fetchAll();
        $currentTitles = array_column($contents, 'title');

        $titles = array_column($array, 'title');
        $bodies = array_column($array, 'body_value');
        // $file = fopen('/Users/brandonsmac/drupal/MyResources-clone/modules/custom/myJSONAPI.1/src/Controller/movieSmall.csv', 'r');
        // $file="1_23.csv";
        $file = '/Users/brandonsmac/drupal/MyResources-clone/modules/custom/myJSONAPI.1/src/Controller/movieSmall.csv';
        $csv= file_get_contents($file);
        $array = array_map("str_getcsv", explode("\n", $csv));
        $json = json_encode($array);
        print_r($json);


        $urls = array_column($array, 'field_source_url');
        $keywords = array_column($array, 'field_keywords');
        //$index = 35;
        //TEST TYPE
        foreach($array as $index=>$value3)
        {
            if(in_array((string)$titles[$index], $currentTitles) == false)
            {
                $node = Node::create(['type' => 'resources']);
                $node->title = (string)$titles[$index];
                //Body can now be an array with a value and a format.
                //If body field exists.
                $body = [
                'value' => (string)$bodies[$index],
                'format' => 'full_html',
                ];
                $node->body = $body;
                $node->set('uid', 1);
                $node->field_source->url = (string)$urls[$index];
                //$node->field_keywords = $array[1]["field_keywords"];
                //CREATES MULTIPLE TAXONOMY TERMS FOR THE NODE
                $str=(string)$keywords[$index];
                $termArray =explode(",",$str);
                foreach($termArray as $tag=>$value4){
                    $node->get('field_keywords')->appendItem(['target_id' => $termArray[$tag]]); 
                    //echo "<h1>".$termArray[$tag]."</h1>";
                }

                $node->status = 1;
                $node->enforceIsNew();
                $flag = $node->isNew();
                if($flag == true){
                    //$node->save();
                    drupal_set_message( "Node with nid " . $node->id() . " saved!\n");
                }
            }
        }

        //$nod = node_load(array('title' => 'How to Create and Add CKEditor Plugins', 'type' => 'resources'));
        // $nodes = \Drupal::entityTypeManager()
        //     ->getStorage('node')
        //     ->loadByProperties(['title' => 'How to Create and Add CKEditor Plugins']);
        //echo "<h>". $nodes[0]->id() . "</h1>";

        return array(
            "#type" => 'markup',
            "#markup" => $html,
        );
    }

    public function deleteResources(){
        //DELETE ALL RESOURCE NODES TO TEST
        $nodes = \Drupal::entityTypeManager()
            ->getStorage('node')
            ->loadByProperties(array('type' => 'resources'));
        foreach ($nodes as $node) {
            $node->delete();
        }
        return array(
            "#type" => 'markup',
            "#markup" => "<p>Resources are Deleted</p>",
        );
    }
}