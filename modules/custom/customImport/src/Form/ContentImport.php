<?php

namespace Drupal\contentimport\Form;

use Drupal\contentimport\Controller\ContentImportController;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

// Use Drupal\Core\Database\Connection; .
// Use Drupal\Core\Entity\ .
/**
 * Configure Content Import settings for this site.
 */
class ContentImport extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contentimport';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'contentimport.settings',
    ];
  }

  /**
   * Content Import Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'contentimport/MyDevel';
    $contentTypes = ContentImportController::getAllContentTypes();
    // $selected = 0;.
    $emptyResults = '<h4>Results</h4>' . "<div id=\"dbTable\" class=\"myAPITable tableFixHead\">" . "<table id=\"dbTable\">";
    $emptyResults .= '<tr><th>NULL</th></tr><tr><td>N/A</td></tr></table></div>';

    $form['contentimport_contenttype'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Content Type'),
      '#options' => $contentTypes,
      '#default_value' => 'Select...',
      '#required' => TRUE,
      '#ajax' => [
        'event' => 'change',
        'callback' => '::contentImportcallback',
        'wrapper' => 'content_import_fields_change_wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    $form['file_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('Upload CSV File to Import'),
      '#size' => 40,
      '#description' => $this->t('Select the CSV file to be imported.'),
      '#required' => FALSE,
      '#autoupload' => TRUE,
      '#upload_validators' => ['file_validate_extensions' => ['csv']],
      /*'#ajax' => [
        'event' => 'change',
        'callback' => '::contentImportTablecallback',
        'wrapper' => 'content_import_table_change_wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],*/
    ];

    $form['loglink'] = [
      '#type' => 'link',
      '#title' => $this->t('Check Log..'),
      '#url' => Url::fromUri('base:sites/default/files/contentimportlog.txt'),
    ];

    $form['import_ct_markup'] = [
      '#suffix' => '<div id="content_import_fields_change_wrapper"></div>',
    ];

    // if($_FILES['files']['tmp_name']['file_upload']){
    // if($form_state->getValue('file_upload')){
    // $form['view'] = [
    // '#type' => 'view',
    // '#value' => $this->t('View'),
    // '#button_type' => 'primary',
    // ];.
    // $form['actions'] = ['#type' => 'actions'];.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
      // @codingStandardsIgnoreStart
      // $xmlPackage['error_code'] = get_default_error_code_value();\
      // '#ajax' => array(
      // 'callback' => '::progressBar',
      // 'wrapper'  => 'progress',
      // 'progress' => array('type' => 'throbber',
      // 'message' => t('TestMessage'),
      // 'interval' => 4,),
      // ),
    ];
    // }
    // @codingStandardsIgnoreEnd
    $form['table_markup'] = [
      '#suffix' => '<div id="content_import_table_change_wrapper"></div>',
    ];

    $form['execute']['output'] = [
      '#type' => 'markup',

      '#markup' => (isset($_SESSION['csv_html_output']) ? $_SESSION['csv_html_output'] : $emptyResults),
      // '#ajax' => [
      // 'event' => 'change',
      // 'callback' => '::contentImportcallback',
      // 'wrapper' => 'content_import_fields_change_wrapper',
      // 'progress' => [
      // 'type' => 'throbber',
      // 'message' => NULL,
      // ],
      // ],
      // '#default_value' => (isset($_SESSION['devel_execute_code']) ?
      // $_SESSION['devel_execute_code'] : ''), !
    ];

    if (isset($_SESSION['csv_html_output'])) {
      unset($_SESSION['csv_html_output']);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Content Import Sample CSV Creation.
   */
  public function contentImportcallback(array &$form, FormStateInterface $form_state) {
    global $base_url;
    $ajax_response = new AjaxResponse();
    $contentType = $form_state->getValue('contentimport_contenttype');
    $fields = ContentImport::getFields($contentType);
    $fieldArray = $fields['name'];
    $contentTypeFields = '';
    /*$contentTypeFields = 'title,';
    $contentTypeFields .= 'langcode,';*/
    $count = 0;
    foreach ($fieldArray as $key => $val) {
      $contentTypeFields .= $val . ',';
      $count = $count + $key;
    }
    $contentTypeFields = substr($contentTypeFields, 0, -1);
    $sampleFile = $contentType . '.csv';
    $handle = fopen("sites/default/files/" . $sampleFile, "w+") or die("There is no permission to create log file. Please give permission for sites/default/file!");
    fwrite($handle, $contentTypeFields);
    $result = '<a class="button button--primary" href="' . $base_url . '/sites/default/files/' . $sampleFile . '">Click here to download Sample CSV</a>';
    $ajax_response->addCommand(new HtmlCommand('#content_import_fields_change_wrapper', $result));
    // Displays the csv file as html on ajax callback
    // $csvTable = ContentImport::createHtmlTable($_FILES, $contentType);
    // $ajax_response->addCommand(
    // new HtmlCommand('#content_import_table_change_wrapper', $csvTable)); !
    return $ajax_response;
  }

  /**
   * Content Import Sample CSV Creation.
   */
  public function contentImportTablecallback(array &$form, FormStateInterface $form_state) {
    // Global $base_url;.
    $ajax_response = new AjaxResponse();
    $contentType = $form_state->getValue('contentimport_contenttype');
    $csvTable = ContentImport::createHtmlTable($_FILES, $contentType);
    $ajax_response->addCommand(new HtmlCommand('#content_import_table_change_wrapper', $csvTable));
    return $ajax_response;
  }

  /**
   * A function that turns our csv file into an array.
   */
  public function createArray($filedata, $contentType) {
    // drupal_flush_all_caches();
    // global $base_url; .
    // $logFileName = "contentimportlog.txt"; .
    // $logFile = fopen("sites/default/files/" . $logFileName, "w") .
    // or die("There is no permission to create log file. .
    // Please give permission for sites/default/file!"); .
    // $fields = ContentImport::getFields($contentType); .
    $location = $filedata['files']['tmp_name']['file_upload'];

    $tmpCsv = [];
    // $file = fopen('/Users/brandonsmac/drupal/MyResources-clone/modules/
    // custom/myJSONAPI.1/src/Controller/movieSmall.csv', 'r'); .
    $file = fopen($location, "r");
    if ($file !== FALSE) {
      while (($result = fgetcsv($file)) !== FALSE) {
        $tmpCsv[] = $result;
      }
      fclose($file);
    }
    // $csv = array_shift($tmpCsv);
    $csv = $tmpCsv;
    return $csv;
  }

  /**
   * Content Import Form Submission.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $contentType = $form_state->getValue('contentimport_contenttype');
    ContentImport::createHtmlTable($_FILES, $contentType);
    $data = ContentImport::mapCsv($_FILES);
    //removing for now to test the table
    /*if ($contentType == "archived") {
      ContentImport::createBasicArticle($data, $contentType);
    }
    elseif ($contentType == "archive_article") {
      ContentImport::createArticle($data, $contentType);
    }
    elseif ($contentType == "company") {
      ContentImport::createCompany($data, $contentType);
    }*/
    if($contentType == "resources"){
      ContentImport::createResources($data, $contentType);
    }
  }

  /**
   * To get all Content Type Fields.
   */
  public static function getFields($contentType) {
    $fields = [];
    foreach (\Drupal::entityManager()
      ->getFieldDefinitions('node', $contentType) as $field_definition) {
      if (!empty($field_definition->getTargetBundle())) {
        $fields['name'][] = $field_definition->getName();
        $fields['type'][] = $field_definition->getType();
        $fields['setting'][] = $field_definition->getSettings();
      }
    }
    return $fields;
  }

  /**
   * To get Reference field ids.
   */
  public function getTermReference($voc, $terms) {
    $vocName = strtolower($voc);
    $vid = preg_replace('@[^a-z0-9_]+@', '_', $vocName);
    $vocabularies = Vocabulary::loadMultiple();
    /* Create Vocabulary if it is not exists */
    if (!isset($vocabularies[$vid])) {
      ContentImport::createVoc($vid, $voc);
    }
    $termArray = array_map('trim', explode(',', $terms));
    $termIds = [];
    foreach ($termArray as $term) {
      $term_id = ContentImport::getTermId($term, $vid);
      if (empty($term_id)) {
        $term_id = ContentImport::createTerm($voc, $term, $vid);
      }
      $termIds[]['target_id'] = $term_id;
    }
    return $termIds;
  }

  /**
   * To Create Terms if it is not available.
   */
  public function createVoc($vid, $voc) {
    $vocabulary = Vocabulary::create([
      'vid' => $vid,
      'machine_name' => $vid,
      'name' => $voc,
    ]);
    $vocabulary->save();
  }

  /**
   * To Create Terms if it is not available.
   */
  public function createTerm($voc, $term, $vid) {
    Term::create([
      'parent' => [$voc],
      'name' => $term,
      'vid' => $vid,
    ])->save();
    $termId = ContentImport::getTermId($term, $vid);
    return $termId;
  }

  /**
   * To Create Terms if it is not available, while missing vid
   */
  // public function createTerm($voc, $term, $vid) {
  //   Term::create([
  //     'parent' => [$voc],
  //     'name' => $term,
  //   ])->save();
  //   $termId = ContentImport::getTermId($term, $vid);
  //   return $termId;
  // }

  /**
   * To get Termid available.
   */
  public function getTermId($term, $vid) {
    $termRes = db_query('SELECT n.tid FROM {taxonomy_term_field_data} n WHERE n.name  = :uid AND n.vid  = :vid', [':uid' => $term, ':vid' => $vid]);
    foreach ($termRes as $val) {
      $term_id = $val->tid;
    }
    return $term_id;
  }


  /**
   * To get Termid available.
   */
  public function getTermId_NoVID($name) {
    $termRes = db_query('SELECT n.tid FROM {taxonomy_term_field_data} n WHERE n.name  = :name', [':name' => $name]);
    foreach ($termRes as $val) {
      $term_id = $val->tid;
    }
    return $term_id;
  }

  /**
   * To get node available.
   */
  public static function getNodeId($title) {
    $nodeReference = [];
    $db = \Drupal::database();
    foreach ($title as $key => $value) {
      $query = $db->select('node_field_data', 'n');
      $query->fields('n', ['nid']);
      $nodeId = $query
        ->condition('n.title', trim($value))
        ->execute()
        ->fetchField();
      $nodeReference[$key]['target_id'] = $nodeId;
    }
    return $nodeReference;
  }

  /**
   * To get user information based on emailIds.
   */
  public static function getUserInfo($userArray) {
    $uids = [];
    foreach ($userArray as $usermail) {
      if (filter_var($usermail, FILTER_VALIDATE_EMAIL)) {
        $users = \Drupal::entityTypeManager()->getStorage('user')
          ->loadByProperties([
            'mail' => $usermail,
          ]);
      }
      else {
        $users = \Drupal::entityTypeManager()->getStorage('user')
          ->loadByProperties([
            'name' => $usermail,
          ]);
      }
      $user = reset($users);
      if ($user) {
        $uids[] = $user->id();
      }
      else {
        $user = User::create();
        $user->uid = '';
        $user->setUsername($usermail);
        $user->setEmail($usermail);
        $user->set("init", $usermail);
        $user->enforceIsNew();
        $user->activate();
        $user->save();
        $users = \Drupal::entityTypeManager()->getStorage('user')
          ->loadByProperties(['mail' => $usermail]);
        $uids[] = $user->id();
      }
    }
    return $uids;
  }

  /**
   * Implements a function to map the headers as data keys from the CSV array.
   */
  public function mapCsv($filedata) {
    $fileName = $filedata['files']['tmp_name']['file_upload'];
    // $fileName = '/Users/brandonsmac/drupal/MyResources-clone/ .
    // modules/custom/myJSONAPI.1/src/Controller/movieSmall.csv'; .
    $row = 1;
    $array = [];
    $marray = [];
    $handle = fopen($fileName, 'r');
    if ($handle !== FALSE) {
      while (($data = fgetcsv($handle, 0, ',')) !== FALSE) {
        if ($row === 1) {
          $num = count($data);
          for ($i = 0; $i < $num; $i++) {
            array_push($array, $data[$i]);
          }
        }
        else {
          $c = 0;
          foreach ($array as $key) {
            $marray[$row - 1][$key] = $data[$c];
            $c++;
          }
        }
        $row++;
      }
      // $handler = fclose($fileName);
      // echo '<pre>';
      // print_r($marray);
      // echo '</pre>';.
    }
    // REINDEX ARRAY TO START AT ZERO.
    $arr = $marray;
    $reIndexed = [];
    $i = 0;
    foreach ($arr as $k => $item) {
      $reIndexed[$i] = $item;
      unset($arr[$k]);
      $i++;
    }
    $swp = $reIndexed;
    $reIndexed = $swp;
    // print_r($array);
    // return $reIndexed;.
    return $marray;
  }

  /**
   * To import data as Content type nodes.
   */
  public function createHtmlTable($filedata, $contentType) {
    // drupal_flush_all_caches();
    // global $base_url; .
    // $logFileName = "contentimportlog.txt"; .
    // $logFile = fopen("sites/default/files/" . $logFileName, "w") .
    // or die("There is no permission to create log file. .
    // Please give permission for sites/default/file!"); .
    // $fields = ContentImport::getFields($contentType); .
    $location = $filedata['files']['tmp_name']['file_upload'];

    $tmpCsv = [];
    // $file = fopen('/Users/brandonsmac/drupal/MyResources-clone/modules/
    // custom/myJSONAPI.1/src/Controller/movieSmall.csv', 'r'); .
    $file = fopen($location, "r");
    if ($file !== FALSE) {
      while (($result = fgetcsv($file)) !== FALSE) {
        $tmpCsv[] = $result;
      }
      fclose($file);
    }
    // $csv = array_shift($tmpCsv);
    $csv = $tmpCsv;
    // Echo '<pre>';
    // print_r($csv);
    // echo '</pre>';.
    $array = $csv;
    // "<div style= 'width: 100% overflow-x: scroll'>" .
    // $html = "<div class=\"myAPITable \"><table>"; .
    $html = '<h4>Results</h4>' . "<div id=\"dbTable\" class=\"myAPITable tableFixHead\">" . "<table id=\"dbTable\" >";
    // Header row.
    $html .= '<tr>';
    $c = 0;
    foreach ($array[0] as $key => $value) {
      $html .= '<th>' . htmlspecialchars($value) . '</th>';
      $c = $c + $key;
    }
    $html .= '</tr>';

    // Remove the first row to avoid it being duplicated.
    $arrRemoved = array_shift($array);
    $dummy = $arrRemoved;
    $dummy = $csv;
    $csv = $dummy;

    // Data rows.
    foreach ($array as $key => $value) {
      $html .= '<tr>';
      $c = 0;
      foreach ($value as $key2 => $value) {
        $html .= '<td>' . htmlspecialchars($value) . '</td>';
        $c = $c + $key2;
      }
      $html .= '</tr>';
    }

    // Finish table and return it.
    $html .= '</table></div>';

    // TESTING TO MAKE SURE DATA IS FORMATTED PROPERLY
    // $newArray = ContentImport::mapCsv($fullArray);
    // $titles = array_column($newArray, 'title');
    // $test = "<pre>".print_r($titles, true)."</pre>";.
    $_SESSION['csv_html_output'] = $html;
    // Return array(
    // "#type" => 'markup',
    // "#markup" => $html,
    // );
    // ContentImport::createArticle($newArray);
    return $html;

  }

  /**
   * Creates a db query to run. You can also swtich to other Datababse.
   *
   * @param string $queryStr
   *   Is the query that you want to execute. Default Value = SELECT DATABASE().
   * @param string $databaseName
   *   The name of the database you want to use. Default = Default DB Name.
   *
   * @return array
   *   A array of stdObjects where each object is a db row.
   */
  public static function dbQuery($queryStr = "SELECT DATABASE()", $databaseName = NULL) {
    $connection = \Drupal::database();
    $dbNamequery = $connection->query("SELECT DATABASE()");
    $getCurretDatabaseName = $dbNamequery->fetchAll();
    $dbs = get_object_vars($getCurretDatabaseName[0]);
    $dbName = array_values($dbs);
    $currentDB = $dbName[0];
    if ($databaseName == NULL) {
      $databaseName = $currentDB;
    }
    // CONNECTS TO DB THAT WAS PROVIDED IF NOT USES CURRENT DB.
    $changeDB = $connection->query("USE " . $databaseName);
    $changed = $changeDB->fetchAll();
    $swp = $changed;
    $changed = $swp;
    // GRABS RESULTS OF QUERY.
    $query = $connection->query($queryStr);
    $results = $query->fetchAll();

    // SWITCHES THE DB BACK TO THE NORMAL DB.
    $defaultDB = $connection->query("USE " . $currentDB);
    $default = $defaultDB->fetchAll();
    $swp = $default;
    $default = $swp;
    return $results;
  }

    /**
   * Public function createResources(&$array, &$context, $contentType)
   */
  public static function createResources(&$array, $contentType) {
    /* If (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $number;
    }.
    // Process the next 100 if there are at least 100 left. Otherwise,
    // we process the remaining number.
    $batch_size = 100;
    $max = $context['sandbox']['progress'] + $batch_size;
    if ($max > $context['sandbox']['max']) {
    $max = $context['sandbox']['max'];
    } */
    // Start where we left off last time.
    // $start = $context['sandbox']['progress']; .
    $prevTitles = '';
    $connection = \Drupal::database();
    $currentContent = $connection->query("SELECT `type`, `title` from node_field_data WHERE type = " . "'" . htmlspecialchars($contentType) . "'");
    $contents = $currentContent->fetchAll();
    $prevTitles = $contents;
    $currentTitles = array_column($prevTitles, 'title');

    // $q = ContentImport::dbQuery("SELECT * FROM node LIMIT 0,2", .
    // "MyResources"); .
    // THE FOLLOWING DISABLES THE ERROR FROM DISPLAYING ON THE PAGE.
    set_error_handler(function () {
      /* ignore errors */
    });
    $data = ContentImport::nestedArraysToArrayOfObjects($array);
    restore_error_handler();

    $k = array_keys($data);
    $a = [];
    foreach ($k as $i => $vall) {
      array_push($a, $data[$i + 1]);
    }
    // $kk = array_keys($a);
    // $vv = array_values($data);
    // REINDEX ARRAY TO START AT ZERO.
    $arr = $data;
    $reIndexed = [];
    $i = 0;
    foreach ($arr as $k => $item) {
      $reIndexed[$i] = $item;
      unset($arr[$k]);
      $i++;
    }
    $swp = $reIndexed;
    $reIndexed = $swp;
    // $data = $reIndexed; .
    $data = $a;

    $titles = array_column($data, 'title');
    $urls = array_column($data, 'url');

    $keywords = array_column($data, 'tags');
    $bodies = array_column($data, 'body_value');


    ini_set('max_execution_time', 3600);
    // THE FOLLOWING DISABLES THE ERROR FROM DISPLAYING ON THE PAGE.
    $flag = 1;
    if ($flag == 2) {
      set_error_handler(function () {
        /* ignore errors */
      });
    }
    foreach ($data as $index => $value3) {
      if (in_array((string) $titles[$index], $currentTitles) == FALSE) {
        $node = Node::create(['type' => 'resources']);
        // $node->set('title', $titles[$index]);

        $node->set('field_source', [
          'url' => $urls[$index],
          // 'width' => $titles[$index],
        ]);

        // BUISNESS CATEGORIES.
        // CREATES MULTIPLE TAXONOMY TERMS FOR THE NODE.
        // $catStr = (string) $keywords[$index];
        // $termArray = explode(", ", $catStr);
        // foreach ($termArray as $tag => $value4) {
        //   $connection = \Drupal::database();
        //   $bcIDQuery = $connection->query("SELECT tid from taxonomy_term_field_data where name = " . "'" . $termArray[$tag] . "'");
        //   $bcID = $bcIDQuery->fetchAll();
        //   $bc = get_object_vars($bcID[0]);
        //   $bcName = array_values($bc);
        //   $node->get('field_keywords')
        //     ->appendItem(['target_id' => $bcName[0]]);
        // }

        $str=(string)$keywords[$index];
        $termArray =explode(", ",$str);
        // foreach($termArray as $tag=>$value4){
        //     $node->get('field_keywords')->appendItem(['target_id' => $termArray[$tag]]); 
        //     //echo "<h1>".$termArray[$tag]."</h1>";
        // }

        // $termArray = array_map('trim', explode(',', $keywords));
        // $termIds = [];
        // foreach ($termArray as $term) {
        //   $term_id = ContentImport::getTermId($term, $vid);
        //   if (empty($term_id)) {
        //     $term_id = ContentImport::createTerm($voc, $term, $vid);
        //   }
        //   $node->get('field_keywords')->appendItem(['target_id' => $termArray[$tag]]); 
        //   // $termIds[]['target_id'] = $term_id;
        // }


        $term_vocab = 'tags'; // Vocabulary machine name
        $terms = $termArray; // List of test terms
        foreach ($terms as $term) {

          //check for term in databaase
          $term_id = ContentImport::getTermId_NoVID($term);
          if (empty($term_id)) {
            $term = Term::create(array(
              'parent' => array(),
              'name' => $term,
              'vid' => $term_vocab,
            ));
            $term->save();
            $term_id = $term->tid->value;
        }
          $node->get('field_keywords')->appendItem(['target_id' => $term_id]); 
        }

        // NAICS Codes.
        // $codeStr = (string) $naics[$index];
        // $codeArray = explode(",", $codeStr);
        // foreach ($codeArray as $code => $value5) {
        //   $node->field_naics[$code] = $codeArray[$code];
        // }

        // Body can now be an array with a value and a format.
        $bodyStr = (string) $bodies[$index];
        // $isHTML != strip_tags($bodyStr) ? true:false; .
        set_error_handler(function () {
          /* ignore errors */
        });
        $isHTML = ContentImport::isHtml($bodyStr);
        restore_error_handler();
        // If body field exists.
        if ($isHTML == TRUE) {
          $body = [
            'value' => (string) $bodies[$index],
            'format' => 'basic_html',
          ];
        }
        elseif ($isHTML == FALSE) {
          $body = $bodyStr;
        }
        $node->body = $body;

        // $node->field_contact_name = $contacts[$index];
        // $node->field_phone = $phones[$index];
        // $node->field_fax = $faxs[$index];
        // $node->field_email = $emails[$index];

        // GENERIC INFORMATION NEEDED FOR NODE CREATION.
        $node->set('uid', 1);
        $node->set('title', $titles[$index]);
        $node->status = 1;
        // $node->set('moderation_state', 'published');
        $node->enforceIsNew();
        $flag = $node->isNew();
        if ($flag == TRUE) {
          $node->save();
        }
      }
      drupal_set_message(t("Nodes have been created"));
    }

    if ($flag == 1) {
      restore_error_handler();
    }
    // $tst = $txt;
    $tst = "<h1>TEST</h1><pre>".print_r($node, true)."</pre>";
    // $tst .= "<h1>TEST</h1>" . $t;
    // $tst .= "<h1>TEST</h1><h2>". $txt ."</h2>";
    // $tst = "<h1>TEST</h1><pre>".print_r($arrTest, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($kk, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($vv, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($data, true)."</pre>";
    // $tst = "<h1>TEST</h1><pre>" . print_r($a, TRUE) . "</pre>";
    $_SESSION['csv_html_output'] = $tst.
    ini_set('max_execution_time', 60);
    return [
      "#type" => 'markup',
      "#markup" => $_SESSION['csv_html_output'],
    ];
  }

  /**
   * Turns an array of arrays into an Array of Objects.
   */
  public static function nestedArraysToArrayOfObjects(&$array) {
    $arrEncode = json_encode($array);
    $arrDecode = json_decode($arrEncode);
    // $bodies = array_column($x, 'title'); .
    $arrObj = get_object_vars($arrDecode);
    return $arrObj;
  }

  /**
   * Public function createArticle(&$array, &$context, $contentType)
   */
  public static function createArticle(&$array, $contentType) {

    $prevTitles = '';
    $connection = \Drupal::database();
    $currentContent = $connection->query("SELECT `type`, `title` from node_field_data WHERE type = " . "'" . htmlspecialchars($contentType) . "'");
    $contents = $currentContent->fetchAll();
    $prevTitles = $contents;
    $currentTitles = array_column($prevTitles, 'title');

    // THE FOLLOWING DISABLES THE ERROR FROM DISPLAYING ON THE PAGE.
    set_error_handler(function () {
      /* ignore errors */
    });
    $data = ContentImport::nestedArraysToArrayOfObjects($array);
    restore_error_handler();

    $k = array_keys($data);
    $buffer = [];
    foreach ($k as $i => $vall) {
      array_push($buffer, $data[$i + 1]);
    }
    // REINDEX ARRAY TO START AT ZERO.
    $arr = $data;
    $reIndexed = [];
    $i = 0;
    foreach ($arr as $k => $item) {
      $reIndexed[$i] = $item;
      unset($arr[$k]);
      $i++;
    }
    $swp = $reIndexed;
    $reIndexed = $swp;
    $data = $buffer;

    $pressReleaseIDs = array_column($data, 'field_press_release_id');
    // $siteIDs = array_column($data, 'field_site_id');.
    $siteNames = array_column($data, 'field_site_name');
    $datelines = array_column($data, 'field_archived_dateline');
    $titles = array_column($data, 'title');
    $bodies = array_column($data, 'field_archived_body');
    $dates = array_column($data, 'field_archived_date');
    $photos = array_column($data, 'field_photo_list');
    $caps = array_column($data, 'field_photo_caption');

    $nodeCount = 0;
    ini_set('max_execution_time', 1800);
    // THE FOLLOWING DISABLES THE ERROR FROM DISPLAYING ON THE PAGE.
    set_error_handler(function () {
      /* ignore errors */
    });
    foreach ($data as $index => $value3) {
      if (in_array((string) $titles[$index], $currentTitles) == FALSE) {
        $nodeCount = $nodeCount + 1;

        $node = Node::create(['type' => 'archive_article']);

        $node->field_press_release_id = $pressReleaseIDs[$index];

        $siteName = $siteNames[$index];
        $connection = \Drupal::database();
        $siteIDQuery = $connection->query("SELECT tid from taxonomy_term_field_data where name = " . "'" . $siteName . "'");
        $siteID = $siteIDQuery->fetchAll();
        $site = get_object_vars($siteID[0]);
        $siteName = array_values($site);
        $node->field_site_id = $siteName;

        $node->field_archived_dateline = $datelines[$index];
        $node->set('title', $titles[$index]);

        // Body can now be an array with a value and a format.
        $bodyStr = (string) $bodies[$index];
        // $isHTML != strip_tags($bodyStr) ? true:false; .
        $isHTML = ContentImport::isHtml($bodyStr);
        // If body field exists.
        if ($isHTML == TRUE) {
          $body = [
            'value' => (string) $bodies[$index],
            'format' => 'rich_text',
          ];
        }
        elseif ($isHTML == FALSE) {
          $body = $bodyStr;
        }
        $node->field_archived_body = $body;

        $dateStr = (string) $dates[$index];
        if (strpos($dateStr, '-') !== FALSE) {
          $splittedString = explode(' ', $dateStr);
          $num = explode('-', $splittedString[0]);
          $date = $num[1] . ' ' . $num[0] . ', 20' . $num[2];
          $node->field_archived_datetime = $date;
        }
        else {
          $node->field_archived_datetime = $dateStr;
        }

        $node->field_photo_list = $photos[$index];
        $node->field_photo_caption = $caps[$index];

        $node->set('uid', 1);
        $node->status = 1;
        $node->set('moderation_state', 'published');
        $node->enforceIsNew();
        $flag = $node->isNew();
        if ($flag == TRUE) {
          $node->save();
          // drupal_set_message( "Node with nid " . $node->id() . " saved!\n");.
        }

      }
      drupal_set_message(t("Nodes have been created"));
    }

    restore_error_handler();
    // $tst = "<pre>".print_r($arrTest[0], true)."</pre>";
    // $tst = "<h1>TEST</h1><pre>".print_r($arrTest, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($kk, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($vv, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($data, true)."</pre>";
    // $tst = "<h1>TEST</h1><pre>" . print_r($buffer, TRUE) . "</pre>";
    // $_SESSION['csv_html_output'] = $tst.
    ini_set('max_execution_time', 60);
    return [
      "#type" => 'markup',
      "#markup" => $_SESSION['csv_html_output'],
    ];
  }

  /**
   * Public function createCompany(&$array, &$context, $contentType)
   */
  public static function createCompany(&$array, $contentType) {
    /* If (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $number;
    }.
    // Process the next 100 if there are at least 100 left. Otherwise,
    // we process the remaining number.
    $batch_size = 100;
    $max = $context['sandbox']['progress'] + $batch_size;
    if ($max > $context['sandbox']['max']) {
    $max = $context['sandbox']['max'];
    } */
    // Start where we left off last time.
    // $start = $context['sandbox']['progress']; .
    $prevTitles = '';
    $connection = \Drupal::database();
    $currentContent = $connection->query("SELECT `type`, `title` from node_field_data WHERE type = " . "'" . htmlspecialchars($contentType) . "'");
    $contents = $currentContent->fetchAll();
    $prevTitles = $contents;
    $currentTitles = array_column($prevTitles, 'title');

    // $q = ContentImport::dbQuery("SELECT * FROM node LIMIT 0,2", .
    // "MyResources"); .
    // THE FOLLOWING DISABLES THE ERROR FROM DISPLAYING ON THE PAGE.
    set_error_handler(function () {
      /* ignore errors */
    });
    $data = ContentImport::nestedArraysToArrayOfObjects($array);
    restore_error_handler();

    $k = array_keys($data);
    $a = [];
    foreach ($k as $i => $vall) {
      array_push($a, $data[$i + 1]);
    }
    // $kk = array_keys($a);
    // $vv = array_values($data);
    // REINDEX ARRAY TO START AT ZERO.
    $arr = $data;
    $reIndexed = [];
    $i = 0;
    foreach ($arr as $k => $item) {
      $reIndexed[$i] = $item;
      unset($arr[$k]);
      $i++;
    }
    $swp = $reIndexed;
    $reIndexed = $swp;
    // $data = $reIndexed; .
    $data = $a;

    $titles = array_column($data, 'title');
    $urls = array_column($data, 'url');
    $add1 = array_column($data, 'address');
    $add2 = array_column($data, 'address2');
    $cities = array_column($data, 'city');
    $st = array_column($data, 'state');
    // $stL = array_column($data, 'state_long');.
    $zips = array_column($data, 'zip');
    $cats = array_column($data, 'categories');
    $naics = array_column($data, 'naics');
    $bodies = array_column($data, 'body');
    $contacts = array_column($data, 'contact');
    $phones = array_column($data, 'phone');
    $faxs = array_column($data, 'fax');
    $emails = array_column($data, 'email');

    ini_set('max_execution_time', 1800);
    // THE FOLLOWING DISABLES THE ERROR FROM DISPLAYING ON THE PAGE.
    $flag = 1;
    if ($flag == 1) {
      set_error_handler(function () {
        /* ignore errors */
      });
    }
    foreach ($data as $index => $value3) {
      if (in_array((string) $titles[$index], $currentTitles) == FALSE) {
        $node = Node::create(['type' => 'company']);
        $node->set('title', $titles[$index]);

        $node->set('field_website', [
          'uri' => $urls[$index],
          'title' => $titles[$index],
        ]);

        $node->set('field_address', [
          'country_code' => 'US',
          'administrative_area' => $st[$index],
          'locality' => $cities[$index],
          'postal_code' => $zips[$index],
          'address_line1' => $add1[$index],
          'address_line2' => $add2[$index],
        ]);

        // BUISNESS CATEGORIES.
        // CREATES MULTIPLE TAXONOMY TERMS FOR THE NODE.
        $catStr = (string) $cats[$index];
        $termArray = explode(", ", $catStr);
        foreach ($termArray as $tag => $value4) {
          $connection = \Drupal::database();
          $bcIDQuery = $connection->query("SELECT tid from taxonomy_term_field_data where name = " . "'" . $termArray[$tag] . "'");
          $bcID = $bcIDQuery->fetchAll();
          $bc = get_object_vars($bcID[0]);
          $bcName = array_values($bc);
          $node->get('field_business_categories')
            ->appendItem(['target_id' => $bcName[0]]);
        }

        // NAICS Codes.
        $codeStr = (string) $naics[$index];
        $codeArray = explode(",", $codeStr);
        foreach ($codeArray as $code => $value5) {
          $node->field_naics[$code] = $codeArray[$code];
        }

        // Body can now be an array with a value and a format.
        $bodyStr = (string) $bodies[$index];
        // $isHTML != strip_tags($bodyStr) ? true:false; .
        set_error_handler(function () {
          /* ignore errors */
        });
        $isHTML = ContentImport::isHtml($bodyStr);
        restore_error_handler();
        // If body field exists.
        if ($isHTML == TRUE) {
          $body = [
            'value' => (string) $bodies[$index],
            'format' => 'rich_text',
          ];
        }
        elseif ($isHTML == FALSE) {
          $body = $bodyStr;
        }
        $node->body = $body;

        $node->field_contact_name = $contacts[$index];
        $node->field_phone = $phones[$index];
        $node->field_fax = $faxs[$index];
        $node->field_email = $emails[$index];

        // GENERIC INFORMATION NEEDED FOR NODE CREATION.
        $node->set('uid', 1);

        $node->status = 1;
        $node->set('moderation_state', 'published');
        $node->enforceIsNew();
        $flag = $node->isNew();
        if ($flag == TRUE) {
          $node->save();
        }
      }
      drupal_set_message(t("Nodes have been created"));
    }

    if ($flag == 1) {
      restore_error_handler();
    }
    // $tst = $txt;
    // $tst = "<h1>TEST</h1><pre>".print_r($data, true)."</pre>";
    // $tst .= "<h1>TEST</h1>" . $t;
    // $tst .= "<h1>TEST</h1><h2>". $txt ."</h2>";
    // $tst = "<h1>TEST</h1><pre>".print_r($arrTest, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($kk, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($vv, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($data, true)."</pre>";
    // $tst = "<h1>TEST</h1><pre>" . print_r($a, TRUE) . "</pre>";
    // $_SESSION['csv_html_output'] = $tst.
    ini_set('max_execution_time', 60);
    return [
      "#type" => 'markup',
      "#markup" => $_SESSION['csv_html_output'],
    ];
  }

  /**
   * Checks the string to see if it contains HTML or not.
   */
  public function isHtml($string) {
    return $string != strip_tags($string) ? TRUE : FALSE;
  }

  /**
   * Deletes Archive Articles. Default Value is Archive Article.
   */
  public static function deleteResources($contentType) {
    // DELETE ALL RESOURCE NODES TO TEST.
    if ($contentType != NULL) {
      $nodes = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties(['type' => 'archive_article']);
      foreach ($nodes as $node) {
        $node->delete();
      }
      return [
        "#type" => 'markup',
        "#markup" => "<p>Resources are Deleted</p>",
      ];
    }
    else {
      return [
        "#type" => 'markup',
        "#markup" => "<p>Please provide a resource to delete.</p>",
      ];
    }
  }

  /**
   * To import data as Content type nodes.
   */
  public function createNode($filedata, $contentType) {
    drupal_flush_all_caches();
    global $base_url;

    $logFileName = "contentimportlog.txt";
    $logFile = fopen("sites/default/files/" . $logFileName, "w") or die("There is no permission to create log file. Please give permission for sites/default/file!");
    $fields = ContentImport::getFields($contentType);
    $fieldNames = $fields['name'];
    $fieldTypes = $fields['type'];
    $fieldSettings = $fields['setting'];
    // Code for import csv file.
    $mimetype = 1;
    if ($mimetype) {
      $location = $filedata['files']['tmp_name']['file_upload'];
      if (($handle = fopen($location, "r")) !== FALSE) {
        $keyIndex = [];
        $index = 0;
        $logVariationFields = "***************************** Content Import Begins ************************************ \n \n ";
        while (($data = fgetcsv($handle)) !== FALSE) {
          $index++;
          if ($index < 2) {
            array_push($fieldNames, 'title');
            array_push($fieldTypes, 'text');
            array_push($fieldNames, 'langcode');
            array_push($fieldTypes, 'lang');
            if (array_search('langcode', $data) === FALSE) {
              $logVariationFields .= "Langcode missing --- Assuming EN as default langcode.. Import continues  \n \n";
              $data[count($data)] = 'langcode';
            }

            foreach ($fieldNames as $fieldValues) {
              $i = 0;
              foreach ($data as $dataValues) {
                if ($fieldValues == $dataValues) {
                  $logVariationFields .= "Data Type : " . $fieldValues . "  Matches \n";
                  $keyIndex[$fieldValues] = $i;
                }
                $i++;
              }
            }
            continue;
          }
          if (!isset($keyIndex['title']) || !isset($keyIndex['langcode'])) {
            drupal_set_message($this->t('title or langcode is missing in CSV file. Please add these fields and import again'), 'error');
            $url = $base_url . "/admin/config/content/contentimport";
            header('Location:' . $url);
            exit;
          }

          $logVariationFields .= "********************************* Importing node ****************************  \n \n";

          for ($f = 0; $f < count($fieldNames); $f++) {
            switch ($fieldTypes[$f]) {
              case 'image':
                $logVariationFields .= "Importing Image (" . trim($data[$keyIndex[$fieldNames[$f]]]) . ") :: ";
                if (!empty($data[$keyIndex[$fieldNames[$f]]])) {
                  $imgIndex = trim($data[$keyIndex[$fieldNames[$f]]]);
                  $files = glob('sites/default/files/' . $contentType . '/images/' . $imgIndex);
                  $fileExists = file_exists('sites/default/files/' . $imgIndex);
                  if (!$fileExists) {
                    $images = [];
                    foreach ($files as $file_name) {
                      $image = File::create(['uri' => 'public://' . $contentType . '/images/' . basename($file_name)]);
                      $image->save();
                      $images[basename($file_name)] = $image;
                      $imageId = $images[basename($file_name)]->id();
                      $imageName = basename($file_name);
                      $swp = $imageName;
                      $imageName = $swp;
                    }
                    $nodeArray = [];
                    $swpArr = $nodeArray;
                    $nodeArray = $swpArr;
                    $nodeArray[$fieldNames[$f]] = [
                      [
                        'target_id' => $imageId,
                        'alt' => $nodeArray['title'],
                        'title' => $nodeArray['title'],
                      ],
                    ];
                    $logVariationFields .= "Image uploaded successfully \n ";
                  }
                }
                $logVariationFields .= " Success \n";
                break;

              case 'entity_reference':
                $logVariationFields .= "Importing Reference Type (" . $fieldSettings[$f]['target_type'] . ") :: ";
                if ($fieldSettings[$f]['target_type'] == 'taxonomy_term') {
                  $reference = explode(":", $data[$keyIndex[$fieldNames[$f]]]);
                  if (is_array($reference) && $reference[0] != '') {
                    $terms = ContentImport::getTermReference($reference[0], $reference[1]);
                    $nodeArray[$fieldNames[$f]] = $terms;
                  }
                }
                elseif ($fieldSettings[$f]['target_type'] == 'user') {
                  $userArray = explode(', ', $data[$keyIndex[$fieldNames[$f]]]);
                  $users = ContentImport::getUserInfo($userArray);
                  $nodeArray[$fieldNames[$f]] = $users;
                }
                elseif ($fieldSettings[$f]['target_type'] == 'node') {
                  $nodeArrays = explode(':', $data[$keyIndex[$fieldNames[$f]]]);
                  $nodeReference1 = ContentImport::getNodeId($nodeArrays);
                  $nodeArray[$fieldNames[$f]] = $nodeReference1;
                }
                $logVariationFields .= " Success \n";
                break;

              case 'text_long':
              case 'text':
                $logVariationFields .= "Importing Content (" . $fieldNames[$f] . ") :: ";
                $nodeArray[$fieldNames[$f]] = [
                  'value' => $data[$keyIndex[$fieldNames[$f]]],
                  'format' => 'full_html',
                ];
                $logVariationFields .= " Success \n";
                break;

              case 'entity_reference_revisions':
              case 'text_with_summary':
                $logVariationFields .= "Importing Content (" . $fieldNames[$f] . ") :: ";
                $nodeArray[$fieldNames[$f]] = [
                  'summary' => substr(strip_tags($data[$keyIndex[$fieldNames[$f]]]), 0, 100),
                  'value' => $data[$keyIndex[$fieldNames[$f]]],
                  'format' => 'full_html',
                ];
                $logVariationFields .= " Success \n";

                break;

              case 'datetime':
                $logVariationFields .= "Importing Datetime (" . $fieldNames[$f] . ") :: ";
                $dateArray = explode(':', $data[$keyIndex[$fieldNames[$f]]]);
                if (count($dateArray) > 1) {
                  $dateTimeStamp = strtotime($data[$keyIndex[$fieldNames[$f]]]);
                  $newDateString = date('Y-m-d\TH:i:s', $dateTimeStamp);
                }
                else {
                  $dateTimeStamp = strtotime($data[$keyIndex[$fieldNames[$f]]]);
                  $newDateString = date('Y-m-d', $dateTimeStamp);
                }
                $nodeArray[$fieldNames[$f]] = ["value" => $newDateString];
                $logVariationFields .= " Success \n";
                break;

              case 'timestamp':
                $logVariationFields .= "Importing Content (" . $fieldNames[$f] . ") :: ";
                $nodeArray[$fieldNames[$f]] = ["value" => $data[$keyIndex[$fieldNames[$f]]]];
                $logVariationFields .= " Success \n";
                break;

              case 'boolean':
                $logVariationFields .= "Importing Boolean (" . $fieldNames[$f] . ") :: ";
                $nodeArray[$fieldNames[$f]] = ($data[$keyIndex[$fieldNames[$f]]] == 'On' ||
                                               $data[$keyIndex[$fieldNames[$f]]] == 'Yes' ||
                                               $data[$keyIndex[$fieldNames[$f]]] == 'on' ||
                                               $data[$keyIndex[$fieldNames[$f]]] == 'yes') ? 1 : 0;
                $logVariationFields .= " Success \n";
                break;

              case 'langcode':
                $logVariationFields .= "Importing Langcode (" . $fieldNames[$f] . ") :: ";
                $nodeArray[$fieldNames[$f]] = ($data[$keyIndex[$fieldNames[$f]]] != '') ? $data[$keyIndex[$fieldNames[$f]]] : 'en';
                $logVariationFields .= " Success \n";
                break;

              case 'geolocation':
                $logVariationFields .= "Importing Geolocation Field (" . $fieldNames[$f] . ") :: ";
                $geoArray = explode(";", $data[$keyIndex[$fieldNames[$f]]]);
                if (count($geoArray) > 0) {
                  $geoMultiArray = [];
                  for ($g = 0; $g < count($geoArray); $g++) {
                    $latlng = explode(",", $geoArray[$g]);
                    for ($l = 0; $l < count($latlng); $l++) {
                      $latlng[$l] = floatval(preg_replace("/\[^0-9,.]/", "", $latlng[$l]));
                    }
                    array_push($geoMultiArray, [
                      'lat' => $latlng[0],
                      'lng' => $latlng[1],
                    ]);
                  }
                  $nodeArray[$fieldNames[$f]] = $geoMultiArray;
                }
                else {
                  $latlng = explode(",", $data[$keyIndex[$fieldNames[$f]]]);
                  for ($l = 0; $l < count($latlng); $l++) {
                    $latlng[$l] = floatval(preg_replace("/\[^0-9,.]/", "", $latlng[$l]));
                  }
                  $nodeArray[$fieldNames[$f]] = ['lat' => $latlng[0], 'lng' => $latlng[1]];
                }
                $logVariationFields .= " Success \n";
                break;

              case 'entity_reference_revisions':
                /* In Progress */
                break;

              default:
                $nodeArray[$fieldNames[$f]] = $data[$keyIndex[$fieldNames[$f]]];
                break;
            }
          }

          if (array_search('langcode', $data) === FALSE) {
            $nodeArray['langcode'] = 'en';
          }

          $nodeArray['type'] = strtolower($contentType);
          $nodeArray['uid'] = 1;
          $nodeArray['promote'] = 0;
          $nodeArray['sticky'] = 0;
          if ($nodeArray['title']['value'] != '') {
            $node = Node::create($nodeArray);
            $node->save();
            $logVariationFields .= "********************* Node Imported successfully ********************* \n\n";
            fwrite($logFile, $logVariationFields);
          }
          $nodeArray = [];
        }
        fclose($handle);
        $url = $base_url . "/admin/content";
        header('Location:' . $url);
        exit;
      }
    } //die('test');
  }

}
