<?php
/**
 * @file
 * contains \Drupal\rsvplist\Plugin\Block\RSVPBlock
 */

 namespace Drupal\rsvplist\Plugin\Block;

 use Drupal\Core\Block\BlockBase;
 use Drupal\Core\Session\AccountInterface;
 use Drupal\Core\Access\AccessResult;
 use Drupal\Core\Form\FormStateInterface;

 /**
  * Provides an 'RSVP' List Block
  * @Block(
  *   id = "recentNode_block",
  *   admin_label = @Translation("Recent Resources"),
  * )
  */

  class RecentNode extends BlockBase {
      /**
       * {@inheritdoc}
       */
      public function build() {
        $array = array();
        $connection = \Drupal::database();
        $query = $connection->query("SELECT title, type, nid from node_field_data where type = 'resources' order by created DESC Limit 0,4");
        $result = $query->fetchAll();

        $array = $result;
        // $url = \Drupal::request()->getSchemeAndHttpHost();

        //Generic Recent Resourse
        $html = '<div class' . '=card-deck' . '>';
        foreach( $array as $key=>$value){
            // $html .= "<div class=\"col-sm-3\" >";//'<div class' . "= col-sm-3 myCard". ' >';
            $html .= "<div class=\"card myCard bg-info text-white \" >";
            foreach($value as $key2=>$value2){
                $nurl = \Drupal::request()->getSchemeAndHttpHost();
                // $base = (string)$nurl;
                if($key2 == 'title'){
                    $html .= "<div class=\"card-body bg-primary\" >" .  htmlspecialchars($value2) . '</div>';
                }
                if($key2 == 'nid') {
                    // $xurl = $nurl + '/node' . '/' . (string)($value2);
                    $html .= '<div class' . '=card-footer' . '>';
                        $html .= '<a href' . '=' . htmlspecialchars($nurl) . "class=\"btn btn-primary\"" . '>' . "View Node" . "</a>";
                    $html .= '</div>';
                }
            }
            // $html .= '</div>';
            $html .= '</div>';
        }
        $html .= '</div>';



        //Custom Block for 5 Recent Nodes with thumbnails. 
        //I havent figured out the thumbnail part yet
        //it does work with images
        //$screenshot = '';
        // $screen_shot_image = '';

        //  $url = "https://www.google.com";
        //  $screen_shot_json_data = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=https://www.google.com");
        //  $screen_shot_result = json_decode($screen_shot_json_data, true);
        //  $screen_shot = $screen_shot_result['screenshot']['data'];
        //  $screen_shot = str_replace(array('_','-'), array('/', '+'), $screen_shot);
        //  $screen_shot_image = "<img src=\"data:image/jpeg;base64,".$screen_shot."\" class='img-responsive img-thumbnail'/>";

        // $html = '<div class' . '=card-group' . '>';
        // foreach( $array as $key=>$value){
        //     $html .= "<div class=\"card bg-dark text-white\" >";//'<div ' . "class=\"bg-dark text-white w-100\"" . '>';
        //     // "<div class=\"bg-dark text-white w-100\" ";
        //     foreach($value as $key2=>$value2){
        //         // $test = "https://stackoverflow.com/questions/1725165/calling-a-function-within-a-class-method";
        //         // $a = $this->Thumbnail($test, "Testing.jpg");
        //         if($key2 == 'nid') {
        //             // $html .= '<td>' . htmlspecialchars($key2) . '</td>';
        //             $html .= "<div class=\"card-img-top\" >";
        //             $image = 'https://images.unsplash.com/18/pups.JPG?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&h=300&fit=crop&ixid=eyJhcHBfaWQiOjF9';
        //             $imageData = base64_encode(file_get_contents($image));
        //             $html.= '<image src' . '=https://images.unsplash.com/18/pups.JPG?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=400&h=300&fit=crop&ixid=eyJhcHBfaWQiOjF9' . " class=\"card-img myThumb\"" . '></iframe>';
        //             // $html.= '<image src' .'=' . $shot . " class=\"card-img myThumb\"" . '></iframe>';
        //             // $html.= '<div>' . $shot . '</div>';
        //             // $html .= $screen_shot_image;
        //             $html.= '</div>';
        //             // $html .= '<img class' . '=card-img' . 'src' . '=data:image/jpeg;base64,'. $imageData . ' alt ' . '=Card image cap' . '>';
        //         }
        //         if($key2 == 'title'){
        //             $html .= '<div class' . '=card-body' . '>';//$html .= '<div class' . '=card-img-overlay' . '>';
        //                 $html .= '<h5 class' . '=card-title' . '>' . $value2 . '</h5>';
        //             $html .= '</div>';
        //         }
        //     }
        //     $html .= '</div>';
        // }
        // $html .= '</div>';


        return array(
            "#type" => 'markup',
            "#markup" => $html,
        );
   
        //   return array('#markup' => $this->t('My RSVP List Block'));
        // $form = \Drupal::formBuilder()->getForm('\Drupal\rsvplist\Form\RSVPForm');
        // return $form;
      }

    //   public function Thumbnail($url, $filename, $width = 150, $height = true) {

    //     // download and create gd image
    //     $image = ImageCreateFromString(file_get_contents($url));
       
    //     // calculate resized ratio
    //     // Note: if $height is set to TRUE then we automatically calculate the height based on the ratio
    //     $height = $height === true ? (ImageSY($image) * $width / ImageSX($image)) : $height;
       
    //     // create image 
    //     $output = ImageCreateTrueColor($width, $height);
    //     ImageCopyResampled($output, $image, 0, 0, 0, 0, $width, $height, ImageSX($image), ImageSY($image));
       
    //     // save image
    //     ImageJPEG($output, $filename, 95); 
       
    //     // return resized image
    //     return $output; // if you need to use it
    //    }
  }