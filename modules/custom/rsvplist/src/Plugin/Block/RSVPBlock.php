<?php
/**
 * @file
 * contains \Drupal\rsvplist\Plugin\Block\RSVPBlock
 */

 namespace Drupal\rsvplist\Plugin\Block;

 use Drupal\Core\Block\BlockBase;
 use Drupal\Core\Session\AccountInterface;
 use Drupal\Core\Access\AccessResult;
 use Drupal\Core\Form\FormStateInterface;

 /**
  * Provides an 'RSVP' List Block
  * @Block(
  *   id = "score_block",
  *   admin_label = @Translation("ScoreCard"),
  * )
  */

  class RSVPBlock extends BlockBase {
      /**
       * {@inheritdoc}
       */
      public function build() {
        //   return array('#markup' => $this->t('My RSVP List Block'));
        $form = \Drupal::formBuilder()->getForm('\Drupal\rsvplist\Form\RSVPForm');
        return $form;
      }
  }