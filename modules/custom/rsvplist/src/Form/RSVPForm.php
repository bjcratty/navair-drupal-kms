<?php
/**
 * @file
 * Contains \Drupal\rsvplist\Form\RSVPForm
 */

namespace Drupal\rsvplist\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an RSVP email form.
 */
class RSVPForm extends FormBase{
    /**
     *  (@inheritdoc)
     */
    public function getFormId() {
        return 'rsvplist_email_form';
    }
    // /**
    //  *  (@inheritdoc)
    //  */
    // public function buildForm(array $form, FormStateInterface $form_state) {
    //     $node = \Drupal::routeMatch()->getParameter('node');
    //     $nid = $node->id->value;
    //     $form['email'] = array(
    //         '#title' => t('Email Address'),
    //         '#type' => 'textfield',
    //         '#size' => 25,
    //         '#description' => t("We'll send an update to the email address you provide."),
    //         '#required' => true,,
    //     );
    //     $form['submit'] = array(
    //         '#type' => 'submit',
    //         '#value' => t('RSVP'),
    //     );
    //     $form['nid'] = array(
    //         '#type' => 'hidden',
    //         '#value' => $nid,
    //     );
    //     return $form;
    // }
    // /**
    //  * (@inheritdoc)
    //  */
    // public function buildForm(array $form, FormStateInterface $form_state) {
    //     $form['employee_name'] = array(
    //       '#type' => 'textfield',
    //       '#title' => t('Employee Name:'),
    //       '#required' => true,,
    //     );
    //     $form['employee_mail'] = array(
    //       '#type' => 'email',
    //       '#title' => t('Email ID:'),
    //       '#required' => true,,
    //     );
    //     // $form['crust_size'] = array(
    //     //     '#title' => t('Crust Size'),
    //     //     '#type' => ''select',',
    //     //     '#description' => ''select', the desired pizza crust size.',
    //     //     '#options' => array(t('--- 'select', ---'), t('10"'), t('12"'), t('16"')),
    //     //   );
    //     $form['actions']['#type'] = 'actions';
    //     $form['actions']['submit'] = array(
    //       '#type' => 'submit',
    //       '#value' => $this->t('Register'),
    //       '#button_type' => 'primary',
    //     );
    //     return $form;
    //   }

    /**
     * (@inheritdoc)
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        // $form['employee_name'] = array(
        //   '#type' => 'textfield',
        //   '#title' => t('Employee Name:'),
        //   '#required' => true,,
        // );
        // $form['employee_mail'] = array(
        //   '#type' => 'email',
        //   '#title' => t('Email ID:'),
        //   '#required' => true,,
        // );
        // $form['crust_size'] = array(
        //     '#title' => t('Crust Size'),
        //     '#type' => ''select',',
        //     '#description' => ''select', the desired pizza crust size.',
        //     '#options' => array(t('--- 'select', ---'), t('10"'), t('12"'), t('16"')),
        //   );
        $form['type_options'] = array (
            '#type' => 'value',
            '#value' => array (
                4 => 'Overarcheived (4)',
                2 => 'Achieved (2)',
                0 => 'Unachieved (0)')
        );

        $form['media_image'] = array(
        '#type'=> 'select',
        '#title'=> 'At least 1 media image?',
        '#options' => $form['type_options']['#value'],
        '#required' => true,
        );
        $form['reference_field'] = array(
        '#type'=> 'select',
        '#title'=> 'Content Type with atleast 1 reference field to custom Voacb',
        '#options' => $form['type_options']['#value'],
        '#required'=> true
        );
        $form['video_embed'] = array(
        '#type'=> 'select',
        '#title'=> 'Video Embed Field exists?',
        '#options' => $form['type_options']['#value'],
        '#required'=> true
        );
        $form['additional_custom_block'] = array(
        '#type'=> 'select',
        '#title'=> 'Add at least 1 additional field to custom block type',
        '#options' => $form['type_options']['#value'],
        '#required'=> true
    );

        $form['custom_block_region'] = array(
        '#type'=> 'select',
        '#title'=> 'Place Custom Block in Region?',
        '#options' => $form['type_options']['#value'],
        '#required'=> true
    );

        $form['comment_type'] = array(
        '#type'=> 'select',
        '#title'=> 'Comment Type and field to comment on an entity other than content?',
        '#options' => $form['type_options']['#value'],
        '#required'=> true
    );

        $form['custom_entity_view_mode'] = array(
        '#type'=> 'select',
        '#title'=> 'Custom Entity View Mode?',
        '#options' => $form['type_options']['#value'],
        '#required'=> true
    );

        $form['custom_menu_with_2_parents_and_1_child'] = array(
        '#type'=> 'select',
        '#title'=> 'Custom Menu with 2 parents and 1 Child?',
        '#options' => $form['type_options']['#value'],
        '#required'=> true,
    );

        $form['place_custom_menu_in_region'] = array(
        '#type'=> 'select',
        '#title'=> 'Place Custom Menu in Region?',
        '#options' => $form['type_options']['#value'],
        '#required'=> true,
    );

        $form['content_creation'] = array(
        '#type'=> 'select',
        '#title'=> 'Create Enough Content?',
        '#options' => $form['type_options']['#value'],
        '#required'=> true
    );

        $form['relationship_view'] = array(
        '#type'=> 'select',
        '#title'=> 'Create View with Relationship?',
        '#options' => $form['type_options']['#value'],
        '#required'=> true,
        );
        $form['contextual_filter'] = array(
        '#type'=> 'select',
        '#title'=> 'Create View with Contextual Filter?',
        '#options' => $form['type_options']['#value'],
        '#required'=> true,
        );
        
        $value = $form_state->getValue('media_image');


        // $form['score'] = array(
        // '#type'=> 'textfield',
        // '#title'=> 'Final Score',
        // '#value' => $value,
        // );
        // $form['name'] = array(
        // '#type'=> 'textfield',
        // '#title'=> 'Final Score',
        // '#value' => $value,
        // );

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Register'),
          '#button_type' => 'primary',
        );
        return $form;
      }
    /**
     * (@inheritdoc)
     */
    public function validateForm(array &$form, FormStateInterface $form_state){
        // $value = $form_state->getValue('email');
        // if ($value == !\Drupal::service('email.validator')->isValid($value)) {
        //     $form_state->setErrorByName('email', t('Email is not valid', array('%mail' => $value)));
        // }
        // $form_state['input']['score'] = 'You entered:' . $form_state['values']['score'];
    }
    /**
     * (@inheritdoc)
     */
    public function submitForm(array &$form, FormStateInterface $form_state){
        //druapal_set_message(t('The form is working.'));
        // $form_state['input']['score'] = 'You entered:' . $form_state['values']['score'];
        // $form_state->getValue('score');

    }
    /**
     * (@inheritdoc)
     */
    // function update_name($form, &$form_state) {
    //     $form = array();
    //     $form['name'] = array(
    //         '#title' => t("Name"),
    //         '#type' => 'textfield',
    //         '#value' => isset($form_state['values']['name']) ? (int)$form_state['values']['name'] + $_SESSION['incr'] : 10,
    //         '#prefix' => "<div id='update-name-value'>",
    //         '#suffix' => "</div>",
    //    );
    //    $form['submit'] = array(
    //         '#type' => 'button',
    //         '#value' => t('Search'),
    //         '#ajax' => array(
    //            'callback' => 'update_name_update',
    //            'wrapper' => 'update-name-value',
    //         ),
    //     );
    //     return $form;
    // }
    // function update_name_update($form, $form_state) {
    //   $incr = 2; // Getting this value by doing a API call (dynamic value)
    //   $_SESSION['incr'] = $incr;  // Passing this value to session
    //   return $form['name'];
    // }
}