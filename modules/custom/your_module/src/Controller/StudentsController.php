<?php
namespace Drupal\your_module\Controller;
use Drupal\Core\Controller\ControllerBase;
class StudentsController extends ControllerBase {

    public function __construct() {

    }

    public function list() {
        $header = array(
            array('data' => t('ID'), 'field' => 'st.id'),
            array('data' => t('Name'), 'field' => 'st.name'),
            array('data' => t('Email'), 'field' => 'st.email'),
        );
        $query = db_select('students', 'st')
        ->fields('st', array('id', 'name', 'email'))
        ->extend('Drupal\Core\Database\Query\TableSortExtender')
        ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
        ->orderByHeader($header);
        $data = $query->execute();
        $rows = array();
        foreach ($data as $row) {
            $rows[] = array('data' => (array) $row);
        }
        $build['table_pager'][] = array(
            '#type' => 'table',
            '#header' => $header,
            '#rows' => $rows,
        );
        $build['table_pager'][] = array(
            '#type' => 'pager',
        );
        return $build;
    }
}