<?php

namespace Drupal\contentUpdate\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for contentUpdate routes.
 */
class ContentUpdateController extends ControllerBase {

  /**
   * Get All Content types.
   */
  public static function getAllContentTypes() {
    $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];
    $contentTypesList['none'] = 'Select';
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    return $contentTypesList;
  }

  // Public function deleteResources() {
  // //DELETE ALL RESOURCE NODES TO TEST
  // $nodes = \Drupal::entityTypeManager()
  // ->getStorage('node')
  // ->loadByProperties(array('type' => 'archive_article'));
  // foreach ($nodes as $node) {
  // $node->delete();
  // }
  // return array(
  // "#type" => 'markup',
  // "#markup" => "<p>Resources are Deleted</p>",
  // );
  // }.
}
