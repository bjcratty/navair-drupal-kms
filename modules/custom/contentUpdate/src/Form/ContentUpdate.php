<?php

namespace Drupal\ContentUpdate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

// Use Drupal\Core\Database\Connection; .
// Use Drupal\Core\Entity\ .
/**
 * Configure Content Import settings for this site.
 */
class ContentUpdate extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contentUpdate';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'contentUpdate.settings',
    ];
  }

  /**
   * Content Import Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'contentUpdate/MyDevel';
    // $contentTypes = ContentUpdateController::getAllContentTypes();
    $contentTypes = [
      'archive_article' => $this->t('Archived Article'),
    ];
    $emptyResults = '<h4>Results</h4>' . "<div id=\"dbTable\" class=\"myAPITable tableFixHead\">" . "<table id=\"dbTable\">";
    $emptyResults .= '<tr><th>NULL</th></tr><tr><td>N/A</td></tr></table></div>';

    $form['contentUpdate_contenttype'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Content Type'),
      '#options' => $contentTypes,
      '#default_value' => 'Select',
      '#required' => TRUE,
    ];

    $form['file_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('Import CSV File'),
      '#size' => 40,
      '#description' => $this->t('Select the CSV file to be imported.'),
      '#required' => FALSE,
      '#autoupload' => TRUE,
      '#upload_validators' => ['file_validate_extensions' => ['csv']],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
      '#button_type' => 'primary',
    ];

    $form['table_markup'] = [
      '#suffix' => '<div id="content_import_table_change_wrapper"></div>',
    ];

    $form['execute']['output'] = [
      '#type' => 'markup',
      '#markup' => (isset($_SESSION['csv_html_output']) ? $_SESSION['csv_html_output'] : $emptyResults),
    ];

    if (isset($_SESSION['csv_html_output'])) {
      unset($_SESSION['csv_html_output']);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * A function that turns our csv file into an array.
   */
  public function createArray($filedata, $contentType) {
    $location = $filedata['files']['tmp_name']['file_upload'];

    $tmpCsv = [];
    $file = fopen($location, "r");
    if ($file !== FALSE) {
      while (($result = fgetcsv($file)) !== FALSE) {
        $tmpCsv[] = $result;
      }
      fclose($file);
    }
    // $csv = array_shift($tmpCsv);
    $csv = $tmpCsv;
    return $csv;
  }

  /**
   * Content Import Form Submission.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $contentType = $form_state->getValue('contentUpdate_contenttype');
    ContentUpdate::createHtmlTable($_FILES, $contentType);
    $data = ContentUpdate::mapCsv($_FILES);
    if ($contentType == "archive_article") {
      ContentUpdate::updateArchiveId($data);
    }
  }

  /**
   * Implements a function to map the headers as data keys from the CSV array.
   */
  public function mapCsv($filedata) {
    $fileName = $filedata['files']['tmp_name']['file_upload'];
    // $fileName = '/Users/brandonsmac/drupal/MyResources-clone/ .
    // modules/custom/myJSONAPI.1/src/Controller/movieSmall.csv'; .
    $row = 1;
    $array = [];
    $marray = [];
    $handle = fopen($fileName, 'r');
    if ($handle !== FALSE) {
      while (($data = fgetcsv($handle, 0, ',')) !== FALSE) {
        if ($row === 1) {
          $num = count($data);
          for ($i = 0; $i < $num; $i++) {
            array_push($array, $data[$i]);
          }
        }
        else {
          $c = 0;
          foreach ($array as $key) {
            $marray[$row - 1][$key] = $data[$c];
            $c++;
          }
        }
        $row++;
      }
      // $handler = fclose($fileName);
      // echo '<pre>';
      // print_r($marray);
      // echo '</pre>';.
    }
    // REINDEX ARRAY TO START AT ZERO.
    $arr = $marray;
    $reIndexed = [];
    $i = 0;
    foreach ($arr as $k => $item) {
      $reIndexed[$i] = $item;
      unset($arr[$k]);
      $i++;
    }
    $swp = $reIndexed;
    $reIndexed = $swp;
    // print_r($array);
    // return $reIndexed;.
    return $marray;
  }

  /**
   * To import data as Content type nodes.
   */
  public function createHtmlTable($filedata, $contentType) {

    $location = $filedata['files']['tmp_name']['file_upload'];

    $tmpCsv = [];
    $file = fopen($location, "r");
    if ($file !== FALSE) {
      while (($result = fgetcsv($file)) !== FALSE) {
        $tmpCsv[] = $result;
      }
      fclose($file);
    }
    $csv = $tmpCsv;

    $array = $csv;

    $html = '<h4>Results</h4>' . "<div id=\"dbTable\" class=\"myAPITable tableFixHead\">" . "<table id=\"dbTable\" >";
    // Header row.
    $html .= '<tr>';
    $c = 0;
    foreach ($array[0] as $key => $value) {
      $html .= '<th>' . htmlspecialchars($value) . '</th>';
      $c = $c + $key;
    }
    $html .= '</tr>';

    // Remove the first row to avoid it being duplicated.
    $arrRemoved = array_shift($array);
    $dummy = $arrRemoved;
    $dummy = $csv;
    $csv = $dummy;

    // Data rows.
    foreach ($array as $key => $value) {
      $html .= '<tr>';
      $c = 0;
      foreach ($value as $key2 => $value) {
        $html .= '<td>' . htmlspecialchars($value) . '</td>';
        $c = $c + $key2;
      }
      $html .= '</tr>';
    }

    // Finish table and return it.
    $html .= '</table></div>';

    $_SESSION['csv_html_output'] = $html;

    return $html;

  }

  /**
   * Creates a db query to run. You can also swtich to other Datababse.
   *
   * @param string $queryStr
   *   Is the query that you want to execute. Default Value = SELECT DATABASE().
   * @param string $databaseName
   *   The name of the database you want to use. Default = Default DB Name.
   *
   * @return array
   *   A array of stdObjects where each object is a db row.
   */
  public static function dbQuery($queryStr = "SELECT DATABASE()", $databaseName = NULL) {
    $connection = \Drupal::database();
    $dbNamequery = $connection->query("SELECT DATABASE()");
    $getCurretDatabaseName = $dbNamequery->fetchAll();
    $dbs = get_object_vars($getCurretDatabaseName[0]);
    $dbName = array_values($dbs);
    $currentDB = $dbName[0];
    if ($databaseName == NULL) {
      $databaseName = $currentDB;
    }
    // CONNECTS TO DB THAT WAS PROVIDED IF NOT USES CURRENT DB.
    $changeDB = $connection->query("USE " . $databaseName);
    $changed = $changeDB->fetchAll();
    $swp = $changed;
    $changed = $swp;
    // GRABS RESULTS OF QUERY.
    $query = $connection->query($queryStr);
    $results = $query->fetchAll();

    // SWITCHES THE DB BACK TO THE NORMAL DB.
    $defaultDB = $connection->query("USE " . $currentDB);
    $default = $defaultDB->fetchAll();
    $swp = $default;
    $default = $swp;
    return $results;
  }

  /**
   * Turns an array of arrays into an Array of Objects.
   */
  public static function nestedArraysToArrayOfObjects(&$array) {
    $arrEncode = json_encode($array);
    $arrDecode = json_decode($arrEncode);
    // $bodies = array_column($x, 'title'); .
    $arrObj = get_object_vars($arrDecode);
    return $arrObj;
  }

  /**
   * Checks the string to see if it contains HTML or not.
   */
  public function isHtml($string) {
    return $string != strip_tags($string) ? TRUE : FALSE;
  }

  /**
   * Update Archived Articles with Archive ID.
   */
  public static function updateArchiveId($array) {
    set_error_handler(function () {
      /* ignore errors */
    });
    $data = ContentUpdate::nestedArraysToArrayOfObjects($array);
    restore_error_handler();

    $k = array_keys($data);
    $buffer = [];
    foreach ($k as $i => $vall) {
      array_push($buffer, $data[$i + 1]);
    }

    $data = $buffer;

    $pressReleaseIDs = array_column($data, 'archive_id');
    $nids = array_column($data, 'nids');

    ini_set('max_execution_time', 1800);
    // THE FOLLOWING DISABLES THE ERROR FROM DISPLAYING ON THE PAGE.
    set_error_handler(function () {
      /* ignore errors */
    });

    // $query = \Drupal::entityQuery('node')
    // ->condition('type', 'archive_article');
    // $nids = $query->execute();
    // $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    // $nodes = $node_storage->loadMultiple($nids);
    // $nodes = \Drupal::entityTypeManager()
    // ->getStorage('node')
    // ->loadByProperties(['type' => 'archive_article',
    // 'title' => $titles[$index]]);
    // $entities = \Drupal::entityTypeManager()->getStorage('node')
    // ->loadByProperties(['type' => 'archive_article']);
    // $str = " ";
    // $txt = " ";
    // $nids = \Drupal::entityQuery('node')->condition('type','archive_article')->execute();
    foreach ($data as $index => $value) {
      // $connection = \Drupal::database();
      // $str = "SELECT nid from node_field_data WHERE
      // type = 'archive_article' AND title = " . "'" . $titles[$index] . "'";
      // $query = $connection->query("SELECT nid from node_field_data
      // WHERE type = 'archive_article'
      // AND title = " . "'" . $titles[$index] . "'");
      // $n = $query->fetchAll();
      // $nodeID = get_object_vars($n[0]);
      // $nid = array_values($nodeID);
      // $nid = 5467;
      // $nodes = \Drupal::entityTypeManager()
      // ->getStorage('node')
      // ->loadByProperties(['type' => 'archive_article',
      // 'title' => $titles[$index]]);
      // $title = $titles[$index];
      // $nodes = \Drupal::entityTypeManager()->getStorage('node')
      // ->loadByProperties(['title' => $title]);.
      $nid = $nids[$index];
      $node = Node::load($nid);
      $node->field_press_release_id = $pressReleaseIDs[$index];
      // $txt .= "<h1>TEST ". $nid ." </h1><h1>".$str."</h1>";
      // $node->set('uid', 1);
      // $node->status = 1;
      // $node->moderation_state= 'published';
      // $node->enforceIsNew();
      // $node->isNew();
      $node->save();
    }

    // $arrTest = [];
    // $nodes = Node::loadMultiple();
    // $i = 0;
    // $nids = \Drupal::entityQuery('node')->condition('type','archive_article')->execute();
    // $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);
    // foreach($nodes as $node){
    // // if($node->type == 'archive_article'){
    // if($node->title == $titles[$i]){
    // $node->field_press_release_id = $pressReleaseIDs[$i];
    // $arrTest[$i] = $pressReleaseIDs[$i];
    // $node->save();
    // $i = $i + 1;
    // }
    // // }
    // // }
    // }.
    drupal_set_message(t("Nodes have been updated"));
    restore_error_handler();
    ini_set('max_execution_time', 60);
    // $tst = $txt;
    // $tst = "<h1>TEST</h1><pre>".print_r($nids, true)."</pre>";
    // // $tst .= "<h1>TEST</h1>" . $t;
    // $tst .= "<h1>TEST</h1><h2>". $txt."</h2>";
    // $tst = "<h1>TEST</h1><pre>".print_r($entities, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($kk, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($vv, true)."</pre>";
    // $tst .= "<h1>TEST</h1><pre>".print_r($data, true)."</pre>";
    // $tst = "<h1>TEST</h1><pre>" . print_r($a, TRUE) . "</pre>";
    // $_SESSION['csv_html_output'] = $tst; .
    return [
      "#type" => 'markup',
      "#markup" => $_SESSION['csv_html_output'],
    ];
  }

  /**
   * Deletes Archive Articles. Default Value is Archive Article.
   */
  public static function deleteResources($contentType) {
    // DELETE ALL RESOURCE NODES TO TEST.
    if ($contentType != NULL) {
      $nodes = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties(['type' => 'archive_article']);
      foreach ($nodes as $node) {
        $node->delete();
      }
      return [
        "#type" => 'markup',
        "#markup" => "<p>Resources are Deleted</p>",
      ];
    }
    else {
      return [
        "#type" => 'markup',
        "#markup" => "<p>Please provide a resource to delete.</p>",
      ];
    }
  }

}
